= Vererbung
Dominikus Herzberg, Christopher Schölzel
:toc: left
:toctitle: Inhaltsverzeichnis
:toclevels: 4
:icons: font
:stylesheet: italian-pop.asciidoc.css
:source-highlighter: coderay
:sourcedir: code/6

include::prelude.adoc[]

Die Aufgaben in diesem Kapitel üben das Programmieren von Klassen mit Vererbung ein.

.Mehr ist besser
****
Uns fehlen noch ein paar gute Aufgaben. Wenn Sie mögen, machen Sie uns Vorschläge!
****

== Equals

Jede Klasse erbt von `Object`, auch wenn dies nicht explizit angegeben wird. Sie haben schon gesehen, dass man das ausnutzen kann um beliebige Objekte auf der Konsole als String auszugeben, indem man die Methode `toString` überschreibt.

Die Methode `equals` ist ebenfalls in `Object` definiert und dient dazu, festzustellen ob zwei Objekte _inhaltlich gleich_ sind (im Gegensatz zum Operator `==`, der auf Identität prüft). Für die Klasse `Point` aus dem vorigen Aufgabensatz wäre z.B. das folgende Verhalten der Methode wünschenswert:

----
jshell> Point p1 = new Point(1,2);
jshell> Point p2 = new Point(1,2);
jshell> Point p3 = new Point(1,3);
jshell> p1.equals(p1)
true
jshell> p1.equals(p2)
true
jshell> p1.equals(p3)
false
----

Implementieren Sie für die Klasse `Point`, `Car` und `Fraction` aus aus dem vorherigen Aufgabensatz eine sinnvolle `equals`-Methode.

[TIP]
====
Die Methode `equals` übernimmt einen Parameter vom Typ `Object`. Das heißt, dass man prinzipiell auch Objekte von verschiedenen Typen vergleichen kann (z.B. `String` und `Car`). Sie müssen also zuerst überprüfen, ob das übergebene Objekt den richtigen Typ für den Vergleich hat. Dazu können Sie den Operator `instanceof` verwenden.

Außerdem werden Sie feststellen, dass man mit einer Variable vom Typ `Object` nicht viel anfangen kann, selbst wenn eigentlich in der Variable ein Objekt steckt, das deutlich mehr Methoden und Felder hat als die Klasse `Object`. Diesen Effekt nennt man _Polymorphismus_. Ein Objekt vom Typ `Car` kann z.B. in unterschiedlichen Formen erscheinen, je nachdem ob es in einer Variable vom Typ `Car` oder vom Typ `Object` steckt. In letzterem Fall nimmt es die Rolle bzw. Gestalt eines `Objects` an und kann auch nur als solches verwendet werden. Um es wieder als `Car` behandeln zu können, muss eine Typumwandlung (engl. _type cast_) stattfinden. Recherchieren Sie, wie die Syntax eines solchen _Casts_ in Java ist.

Das Thema der "Gleichheit" von Objekten finden Sie ausführlich dargelegt im Buch "Java ist auch eine Insel" (Rheinwerk-Verlag) von Christian Ullenboom. Das relevante Kapitel finden Sie http://openbook.rheinwerk-verlag.de/javainsel9/javainsel_09_003.htm#t2t34[hier] in der http://openbook.rheinwerk-verlag.de/javainsel9/[Openbook-Fassung].
====

include::preDetailsSolution.adoc[]
[source, java]
----
include::{sourcedir}\equals.java[]
----
<1> Da der Parameter von `equals` vom Typ `Object` ist, muss zuerst geprüft werden ob es sich bei dem übergebenen Argument überhaupt um ein Objekt vom richtigen Typ handelt.
<2> Nachdem wir überprüft haben, dass das Objekt den richtigen Typ hat können wir es zu diesem Typ _casten_, um auf die relevanten Felder zugreifen zu können.
<3> Eine Grundregel von Java lautet "Wer `equals` sagt, muss auch `hashCode` sagen.". Überschreibt man die Funktionalität von `equals`, muss man gewährleisten, dass für zwei Objekte für die `x.equals(y)` den Wert `true` ergibt auch `x.hashCode() == y.hashCode()` gelten muss. Eine einfache Lösung dafür ist die Hilfsmethode `hash()` der Klasse `java.util.Objects`. An diese Methode müssen alle Objekte übergeben werden, die bei dem Vergleich relevant sind. 
include::postDetails.adoc[]

== Mediaplayer

Eine Mediaplayer-Software bildet die abgespielten Medien mit den Klassen `Video` und `Album` ab.

[source, java]
----
include::{sourcedir}\videoAudio.java[]
----

Erklären Sie dem Entwickler dieser Software warum es eine eher schlechte Idee sein könnte, die beiden Medienformate als vollkommen getrennte Klassen zu behandeln und entwickeln Sie eine gemeinsame Überklasse `Medium`, die möglichst viele Gemeinsamkeiten der beiden Klassen vereint. Schreiben Sie die Klassen `Video` und `Album` so um, dass sie von `Medium` erben und dass möglichst wenig Code in den Klassen `Video` und `Album` verbleibt.

include::preDetailsSolution.adoc[]
[source, java]
----
include::{sourcedir}\Medium.java[]
----
include::postDetails.adoc[]


== Typen im Sammelkartenspiel

Sie wollen ein Sammelkartenspiel wie https://playhearthstone.com/de-de/[Heartstone] oder https://www.playgwent.com/en[Gwent] implementieren.
Für Ihr Spiel haben Sie die folgenden Designentscheidungen getroffen:

* Alle Karten haben eine Seltenheit (`COMMON`, `RARE` oder `LEGENDARY`) und einen Namen.
* Es gibt Zauberkarten und Einheitenkarten.
* Einheitenkarten haben zusätzlich eine Stärke zwischen 1 und 100.
* Zauberkarten unterscheiden sich zusätzlich dadurch, ob sie auf freundliche Einheiten, feindliche Einheiten, keine (für globale Effekte) oder beide angewendet werden können.

Erstellen Sie die Klassen `SpellCard` und `UnitCard` für Zauber- und Einheitenkarten, sowie eine gemeinsame Oberklasse `Card`, so dass alle oben genannten Eigenschaften mit geeigneten Datentypen abgebildet werden.

include::preDetailsSolution.adoc[]
----
enum Rarity { COMMON, RARE, LEGENDARY }

abstract class Card {
  private String name;
  private Rarity rarity;
  public Card(String name, Rarity rarity) {
    this.name = name;
    this.rarity = rarity;
  }
  public String getName() { return name; }
  public Rarity getRarity() { return rarity; }
}

class UnitCard extends Card {
  private int strength;
  public UnitCard(String name, Rarity rarity, int strength) {
    super(name, rarity);
    this.strength = strength;
  }
  public int getStrength() {
    return strength;
  }
}

class SpellCard extends Card {
  private boolean targetsEnemies;
  private boolean targetsFriends;
  public SpellCard(String name, Rarity rarity, boolean targetsEnemies, boolean targetsFriends) {
    super(name, rarity);
    this.targetsEnemies = targetsEnemies;
    this.targetsFriends = targetsFriends;
  }
  public boolean targetsFriends() {
    return targetsFriends;
  }
  public boolean targetsEnemies() {
    return targetsEnemies;
  }
}
----
include::postDetails.adoc[]


== SVG Generator

Das https://en.wikipedia.org/wiki/Scalable_Vector_Graphics[Scalable Vector Graphics]-Format beschreibt Vektorgrafiken als XML-basierte Textdatei, wie in dem folgenden Beispiel:

----
<svg width="120" height="120" viewBox="0 0 120 120"
    xmlns="http://www.w3.org/2000/svg">
  <circle cx="60" cy="60" r="50.5" style="fill:yellow"/>
  <line x1="20" y1="100" x2="100" y2="20" style="stroke-width: 2; stroke:black"/>
</svg>
----

Schreiben Sie die Klassen `SVGDocument`, `SVGLine` und `SVGCircle`, so dass das oben gezeigte Dokument (bzw. ein äquivalentes Dokument) mit dem folgenden Code erzeugt werden kann.
Implementieren Sie dazu auch eine gemeinsame Überklasse für `SVGLine` und `SVGCircle` mit dem Namen `SVGElement`.

----
SVGDocument doc = new SVGDocument(120, 120,
    new SVGCircle(60,60,50.5f, "fill:yellow"),
    new SVGLine(20,100,100,20, "stroke-width: 2; stroke:black")
);
System.out.println(doc);
----

include::preDetailsSolution.adoc[]
----
class SVGDocument {
  private float w,h;
  private SVGElement[] content;
  public SVGDocument(float w, float h, SVGElement ... content) {
    this.w = w;
    this.h = h;
    this.content = content;
  }
  public String toString() {
    String msg = "<svg width=\"%1$.2f\" height=\"%2$.2f\" viewBox=\"0 0 %1$.2f %2$.2f\" "
                 + "xmlns=\"http://www.w3.org/2000/svg\">\n%3$s\n</svg>";
    String contentString = Arrays.stream(content)
        .map(x -> x.toString()).collect(Collectors.joining("\n"));
    return String.format(Locale.US, msg, w, h, contentString);  // <1>
  }
}

abstract class SVGElement {
  private String name;
  private String style;
  public SVGElement(String name, String style) {
    this.name = name;
    this.style = style;
  }
  public String toString() {
    String attr = additionalAttributes();
    return String.format("<%1$s %3$s style=\"%2$s\"/>", name, style, attr);
  }
  protected abstract String additionalAttributes();
}

class SVGLine extends SVGElement {
  private float x1,y1,x2,y2;
  public SVGLine(float x1, float y1, float x2, float y2, String style) {
    super("line", style);
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }
  @Override
  protected String additionalAttributes() {
    String tmpl = "x1=\"%.2f\" y1=\"%.2f\" x2=\"%.2f\" y2=\"%.2f\"";
    return String.format(Locale.US, tmpl, x1, y1, x2, y2);  // <1>
  }
}

class SVGCircle extends SVGElement {
  private float cx,cy,r;
  public SVGCircle(float cx, float cy, float r, String style) {
    super("circle", style);
    this.cx = cx;
    this.cy = cy;
    this.r = r;
  }
  @Override
  protected String additionalAttributes() {
    String tmpl = "cx=\"%.2f\" cy=\"%.2f\" r=\"%.2f\"";
    return String.format(Locale.US, tmpl, cx, cy, r);   // <1>
  }
}
----
<1> Die `Locale` ist hier wichtig, da das SVG-Format vorschreibt, dass Fließkommazahlen in englischer Schreibweise (mit "." statt ",") formatiert werden müssen.
include::postDetails.adoc[]
