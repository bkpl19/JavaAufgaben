# Java-Aufgaben

In diesem Repository finden Sie zahlreiche Aufgaben zur Java-Programmierung. Die Aufgaben sind konzeptioniert für die Veranstaltung "Objektorientierte Programmierung (OOP)" an der [Technischen Hochschule Mittelhessen](https://www.thm.de) im [Fachbereich MNI](https://www.thm.de/mni/) bei [Prof. Dr. Herzberg](https://www.thm.de/mni/dominikus-herzberg).

## Lernen Sie mit den Aufgaben!

Für das WS2019/20 stehen einige Pools an Aufgaben zur Verfügung. Teils haben Sie auch Zugriff auf die Lösungen -- und das ist gewollt. Denn es gibt viele Arten des Lernens: Man kann selber tüfteln, bis man die Lösung hat. Dann hätte man gerne vielleicht einen Lösungsvorschlag zum Vergleich. (Ich scheue das Wort "Musterlösung", weil es in der Programmierung oft mehrere Möglichkeiten der Lösung gibt, was auch abhängt von den Programmierkonstrukten, die sie beherrschen.) Manchmal ist es auch sinnvoll, frühzeitig in die Lösung zu schauen, damit man sich abgucken kann, wie das Programmieren funktioniert. Es ist nicht sinnvoll, wenn man über Stunden auf dem Schlauch steht. Das "Spicken" in die Lösung kann eine große Hilfe und eine sehr effektive Lernstrategie sein. Mit zunehmenden Aufgaben kommt man so eigenständig weiter und probiert es alleine.

Eines möchte ich besonders betonen: Die Lerneffekte können enorm sein, wenn Sie in der Gruppe arbeiten. Sie diskutieren, probieren, geben sich gegenseitig Hilfestellung und Anregung und können Lösungen oder Lösungsideen vergleichen. Wenn Sie es schaffen, eine gute Programmiergruppe zu sein, dann ist das gleichzeitig eine hervorragende Lerngruppe. In der Praxis ist Entwicklungsarbeit längst zu einem "Teamsport" geworden -- dafür sind die Aufgaben und Herausforderungen, vor denen die Entwickler:innen stehen, einfach zu groß als dass man alleine zum Ziel käme.

Die Angabe der Vorlesungswoche (VW) gibt an, in welcher Woche Sie sich ab Freitag in Ihrer Studiengruppe mit diesen Aufgaben beschäftigen sollen. In der Folgewoche sind die Delegierten dann angehalten, sich mit den Tutoren zu treffen.

> Die Planung der Aufgaben kann sich möglicherweise noch ändern. Auch sind Korrekturen, Lösungsvorschläge und vor allem Ideen für Aufgaben gerne willkommen.

Viel Erfolg! -- Ihr Dominikus Herzberg

## Grundlegendes bis hin zur Methode

<!-- * [Formale Sprachen und Grammatiken](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/1.FormaleGrammatik.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/1.FormaleGrammatik.adoc)) -->


* **VW1**: [Grundlagen und Arrays](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/2.GrundlagenUndArrays.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/2.GrundlagenUndArrays.adoc)) -->
* **VW2**: [Aufgaben mit Methode](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/3.AufgabenMitMethode.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/3.AufgabenMitMethode.adoc)) -->
* **VW3**: [Zeichenketten](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/4.Zeichenketten.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/4.Zeichenketten.adoc)) -->

## Es geht los mit OO-Konzepten

* **VW4**: [Klassen](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/5.Klassen.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/5.Klassen.adoc)) -->
* **VW5**: [Vererbung](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/6.Vererbung.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/6.Vererbung.adoc)) -->
* **VW6**: [Enumerationen](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/7.Enumerationen.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/7.Enumerationen.adoc)) -->
* **VW7**: [Interfaces](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/8.Interfaces.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/8.Interfaces.adoc)) -->
* **VW8**: [Exceptions](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/9.Exceptions.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/9.Exceptions.adoc)) -->

## Die hohe Schule der Objektorientierung

* **VW9**: [Generics](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/10.Generics.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/10.Generics.adoc)) -->
* **VW10**: [Collections](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/11.Collections.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/11.Collections.adoc)) -->

## Outtakes

Die hier vorgestellten Outtakes sind nicht direkt prüfungsrelevant. Der Code ist schwieriger und anspruchsvoller als das, was Sie in den ersten Wochen zu Java lernen. Gegen Ende des Semesters sollten Sie jedoch weitaus besser mit den Outtakes zurecht kommen.

* [Outtakes](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/Outtakes.html) <!-- ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/Outtakes.adoc)) -->

## Allgemeine Hinweise

* Die meisten Aufgaben wurden erstellt mit dem _Early Access Release_ für Java 9, mittlerweile ist Java 17 aktuell. Wenn es mit Lösungen Probleme gibt, dann teilen Sie das den Tutoren mit.

* Zur Anzeige der HTML-Versionen der generierten HTML-Seiten wird http://htmlpreview.github.io/ verwendet. Hin und wieder ist der Server überlastet. Zur Not hilft, sich die Dateien des Repositories einfach herunterzuladen und sie sich lokal auf dem Rechner anzuschauen.

* Wenn Sie sich auskennen und die HTML-Dateien selber aus den `adoc`-Dateien und den Programmcode-Dateien aus dem `code`-Verzeichnis generieren wollen, benötigen Sie eine Installation von [Asciidoctor](https://asciidoctor.org) auf Ihrem Rechner. Die Überetzung ist ganz einfach. Beispiel: `asciidoctor 3.AufgabenMitMethode.adoc`

### Logo-Quelle

Das [Logo](https://pixabay.com/de/caf%C3%A9-java-logo-kaffee-151346/) stammt aus Pixabay und ist frei nutzbar.