class Fraction {
    // https://en.wikipedia.org/wiki/Greatest_common_divisor
    static long gcd(long a, long b) {
        if (b == 0) return Math.abs(a);
        return gcd(b, a % b);
    }
    // https://en.wikipedia.org/wiki/Least_common_multiple
    static long lcm(long a, long b) {
        return Math.abs(a * b)/gcd(a,b);
    }

    private final long numerator;
    private final long denominator;

    Fraction(long numerator) {
        this(numerator, 1L);
    }

    Fraction(long numerator, long denominator) {
        if (denominator == 0L)
            throw new IllegalArgumentException("denominator == 0");
        long reduction = gcd(numerator, denominator);
        long sign = (long) Math.signum(denominator);
        this.numerator = sign * numerator/reduction;
        this.denominator = sign * denominator/reduction;
    }

    Fraction add(Fraction f) {
        long lcm = lcm(this.denominator, f.denominator);
        return new Fraction(
            lcm/this.denominator*this.numerator +
            lcm/f.denominator*f.numerator,
            lcm);
    }

    Fraction neg() {
        return new Fraction(-numerator,denominator);
    }

    Fraction inv() {
        return new Fraction(denominator,numerator);
    }

    Fraction sub(Fraction f) {
        return this.add(f.neg());
    }

    Fraction mul(Fraction f) {
        return new Fraction(
            this.numerator * f.numerator,
            this.denominator * f.denominator
        );
    }

    Fraction div(Fraction f) {
        return this.mul(f.inv());
    }

    public String toString() {
        String s = "";
        long whole = numerator / denominator;
        long rest  = numerator % denominator;
        if (rest == 0) return s + whole;
        if (whole != 0) s += whole + " " + Math.abs(rest);
        else s += numerator;
        return s + "/" + denominator;
    }
}
