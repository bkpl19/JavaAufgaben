import java.util.Set;
import java.util.HashSet;

void printAll(Set<Integer> set) {
  for(Integer x : set) {
    printf("%d ", x);
  }
  printf("\n");
}

Set<Integer> randomDistinct(int n) {
  Random r = new Random();
  int count = 0;
  Set<Integer> set = new HashSet<Integer>();
  while ( set.size() < n) {
    int ri = r. nextInt(100);
    set.add(ri);
  }
  return set;
}
