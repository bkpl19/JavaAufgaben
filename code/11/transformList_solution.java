import java.util.List;
import java.util.ArrayList;

List<Double> rand(int n) {
  List<Double> lst = new ArrayList<Double>(n);
  while(lst.size() < n) {
    lst.add(Math.random());
  }
  return lst;
}

<E> void swap(List<E> lst, int i1, int i2) {       // <1>
  E tmp = lst.get(i1);
  lst.set(i1, lst.get(i2));
  lst.set(i2, tmp);
}

List<Integer> keepPositives(List<Integer> lst) {
  List<Integer> lstp = new ArrayList<>();         // <2>
  for(int x : lst) {
    if(x >= 0) {
      lstp.add(x);
    }
  }
  return lstp;
}

// kürzere Implementierung mit Streams:
import java.util.stream.Collectors;
List<Integer> keepPositives(List<Integer> lst) {
  return lst.stream().filter(x -> x >= 0).collect(Collectors.toList());
}