int parseDecimal(String s) throws NumberFormatException {
//  tag::headless[]
    if (s == null)
        throw new NumberFormatException("null");
    if (s.isEmpty())
        throw new NumberFormatException("For input string: \""+ s + "\"");

    int factor, from;
    switch (s.charAt(0)) {
        case '-': factor = -1; from = 1; break;
        case '+': factor = +1; from = 1; break;
        default : factor = +1; from = 0;
    }
    String r = s.substring(from);

    if (r.isEmpty())
        throw new NumberFormatException("For input string: \""+ s + "\"");
    if (r.charAt(0) == '0' && r.length() > 1)
        throw new NumberFormatException("\"" + s + "\" has leading zeros");
    int length = r.length();
    char[] digits = new char[length];
    for(int i = 0; i < length; i++)
        digits[i] = r.charAt(length-1-i);

    int number = 0;
    for(char digit : digits) {
        if (factor == 1_000_000_000 * 10 || factor == -1_000_000_000 * 10)
            throw new NumberFormatException("For input string: \""+ s + "\"");
        if (!(digit >= '0' && digit <= '9'))
            throw new NumberFormatException("For input string: \""+ s + "\"");
        number += factor * (digit - '0');
        if (Math.signum(number) == -Math.signum(factor))
            throw new NumberFormatException("For input string: \""+ s + "\"");
        factor *= 10;
    }
    return number;
}
//  end::headless[]

// tag::testSimple[]
List<String> data = List.of("17", "0", "1", "999", "1024", String.valueOf(Integer.MAX_VALUE));
for(String input : data) {
    assert parseDecimal(input) == Integer.parseInt(input);
    assert parseDecimal("+" + input) == Integer.parseInt("+" + input);
    assert parseDecimal("-" + input) == Integer.parseInt("-" + input);
}
assert parseDecimal(String.valueOf(Integer.MIN_VALUE)) ==
       Integer.parseInt(String.valueOf(Integer.MIN_VALUE));
// end::testSimple[]

// tag::testRandomly[]
Random rand = new Random();
for(int i = 1; i <= 1000; i++) {
    int n = rand.nextInt();
    assert parseDecimal(String.valueOf(n)) == n;
    if (n > 0) assert parseDecimal("+" + String.valueOf(n)) == n;
}
// end::testRandomly[]

// tag::testMyExceptionCode[]
boolean testMyException(String decimal, String message) {
    try {
        parseDecimal(decimal);
        return false;
    } catch (NumberFormatException e) {
        return e.getMessage().equals(message);
    }
}
// end::testMyExceptionCode[]

// tag::testMyException[]
assert testMyException("009", "\"009\" has leading zeros");
assert testMyException("+009", "\"+009\" has leading zeros");
assert testMyException("-009", "\"-009\" has leading zeros");
// end::testMyException[]

// tag::testExceptionCode[]
boolean testException(Class exceptionClass, String decimal) {
    try {
        parseDecimal(decimal);
        return false;
    } catch (Throwable e1) {
        if (!exceptionClass.isInstance(e1)) return false; 
        try {
            Integer.parseInt(decimal);
            return false;
        } catch (Throwable e2) {
            if (!exceptionClass.isInstance(e2)) return false; 
            return e1.getMessage().equals(e2.getMessage());
        }
    }
}
// end::testExceptionCode[]

// tag::testExceptionFirst[]
assert testException(NumberFormatException.class, null);
// end::testExceptionFirst[]
// tag::testExceptionRest[]
assert testException(NumberFormatException.class, "");
assert testException(NumberFormatException.class, "+");
assert testException(NumberFormatException.class, "-");
assert testException(NumberFormatException.class, "_");
assert testException(NumberFormatException.class, "1_024");
assert testException(NumberFormatException.class, "2147483648");
assert testException(NumberFormatException.class, "-2147483649");
assert testException(NumberFormatException.class, "1_000_000_000_0");
assert testException(NumberFormatException.class, "-1_000_000_000_0");
assert testException(NumberFormatException.class, "12c3");
// end::testExceptionRest[]

/* Ein Fehler! 
// tag::bug[]
jshell> parseDecimal("5000000000")
$501 ==> 705032704
// end::bug[]
*/