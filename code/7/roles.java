class User {
  static final int ACCESS_GUEST = 0;
  static final int ACCESS_ADMIN = 2;
  String name;
  int access;
  User(String name, int access) {
    this.name = name;
    this.access = access;
  }
  boolean canUploadFiles() {
    switch(access) {
      case ACCESS_GUEST:
        return false;
      default:
        return true;
    }
  }
}
