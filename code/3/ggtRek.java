int ggt(int a, int b) {
  if(b == 0) return a;
  return ggt(b, a % b);
}