int sumFromTo(int x, int y) {
  if(y == x) return 0;
  return x + sumFromTo(x+1, y);
}