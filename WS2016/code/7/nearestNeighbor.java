import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;

class NearestNeighborClassifier {
  double[][] database;
  String[] labels;
  NearestNeighborClassifier() {
    this.database = new double[0][0];
  }
  void train(double[][] database, String[] labels) {            // <1>
    this.database = database;
    this.labels = labels;
  }
  String predict(double[] sample) {
    String label = "";
    double minDist = Double.POSITIVE_INFINITY;
    for(int i = 0; i < database.length; i++) {
      double cur = dist(database[i], sample);
      if (cur < minDist) {
        label = labels[i];
        minDist = cur;
      }
    }
    return label;
  }
  double dist(double[] a, double[] b) {
    double sum = 0;
    for(int i = 0; i < a.length; i++) {
      sum += Math.pow(a[i]-b[i],2);
    }
    return Math.sqrt(sum);
  }
}

String[][] readCSV(Path fname, final String separator) throws IOException {
  return Files
         .lines(fname, StandardCharsets.UTF_8)                // <2>
         .map(x -> x.split(separator))
         .toArray(String[][]::new);                           // <3>
}

String[] extractLabels(String[][] raw) {
  return Arrays.stream(raw).map(x -> x[1]).toArray(String[]::new);
}

double[][] extractAttributes(String[][] raw) {
  return Arrays
         .stream(raw)
         .map(x -> Arrays
                   .stream(x)
                   .skip(2)
                   .mapToDouble(Double::parseDouble)
                   .toArray())
         .toArray(double[][]::new);
}

double[][] filterAttributes(double[][] attributes, int... indices) {
  return Arrays
         .stream(attributes)
         .map(x -> Arrays
                   .stream(indices)
                   .mapToDouble(i -> x[i])
                   .toArray())
         .toArray(double[][]::new);
}

double testNN(Path dbname) {
  String[][] raw;
  try {
    raw = readCSV(dbname, ",");
  } catch (IOException ioe) {
    return 0;
  }
  String[] labels = extractLabels(raw);
  double[][] attributes = extractAttributes(raw);
  attributes = filterAttributes(attributes, 23, 24, 2, 22, 3, 4);
  int nTrain = (int) Math.round(labels.length * 0.7);
  String[] labelsTrain = Arrays.stream(labels)
                         .limit(nTrain).toArray(String[]::new);
  String[] labelsTest = Arrays.stream(labels)
                        .skip(nTrain).toArray(String[]::new);
  double[][] attrTrain = Arrays.stream(attributes)
                         .limit(nTrain).toArray(double[][]::new);
  double[][] attrTest = Arrays.stream(attributes)
                        .skip(nTrain).toArray(double[][]::new);
  NearestNeighborClassifier classifier = new NearestNeighborClassifier();
  classifier.train(attrTrain, labelsTrain);
  int correct = 0;
  for(int i = 0; i < attrTest.length; i++) {
    String pred = classifier.predict(attrTest[i]);
    if (labelsTest[i].equals(pred)) correct++;
  }
  return 1.0 * correct / attrTest.length;
}

double acc = testNN(Paths.get("exercises/data/7/wdbc.data"));
printf("%.0f%% richtig klassifiziert.\n", acc * 100)