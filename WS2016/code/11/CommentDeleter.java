import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
class CommentDeleter {
  public static void main(String[] args) {
    try (
      BufferedReader br = Files.newBufferedReader(
        Paths.get(args[0]), StandardCharsets.UTF_8
      );
      BufferedWriter bw = Files.newBufferedWriter(
        Paths.get(args[1]), StandardCharsets.UTF_8
      )
    ) {
      String line;
      while((line = br.readLine()) != null) {
        if (!line.startsWith("#")) {
          bw.write(line);
          bw.write("\n");
        }
      }
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }
}
