String changeName(String fileName, int n) {
    Pattern p = Pattern.compile("(.*)(\\..*)");
    Matcher m = p.matcher(fileName);
    return m.find() ? m.group(1) + n + m.group(2) : fileName + n;
}

/*
jshell> changeName("test.it.txt",3)
$65 ==> "test.it3.txt"

jshell> changeName("real.ity",2)
$66 ==> "real2.ity"

jshell> changeName("a_name",2)
$67 ==> "a_name2"

jshell> changeName("real.ity.",2)
$68 ==> "real.ity2."

jshell> changeName(".test",2)
$69 ==> "2.test"
*/
