interface NamedAnimal {
  String getName();
  int getWeight();
}

class Rabbit implements NamedAnimal {
        int weight = 1;
        String name;
        Rabbit(String name) { this.name = name; }
        void eat() { weight += 1; }
 public int getWeight() { return weight; }
 public String getName() { return name; }
}
class Wolf  implements NamedAnimal {
        int weight = 1;
        String name;
        Wolf(String name) { this.name = name; }
        void eat(Rabbit r) { weight += r.getWeight(); }
 public int getWeight() { return weight; }
 public String getName() { return name; }
}
