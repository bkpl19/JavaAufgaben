enum Op {
    PLUS, TIMES;
    
    public String toString() {
        switch (this) {
            case PLUS: return "+";
            case TIMES: return "*";
            default: return "???";
        }
    }    
}

class Expression {
    static boolean infix = true;
}

class Num extends Expression {
    Integer value;
    Num(Integer value) {
        this.value = value;
    }
    public String toString() {
        return value.toString();
    }
}

class BinaryOperation extends Expression {
    Expression left, right;
    Op operator;
    BinaryOperation(Expression e1, Op op, Expression e2) {
        left = e1;
        right = e2;
        operator = op;
    }
    public Op getOperator() { return operator; }
    public Expression getLeft() { return left; }
    public Expression getRight() { return right; }

    public String toString() {
        String str = "";
        if (infix) {
            if (operator == Op.TIMES && left instanceof Plus)
                str += "(" + left + ")";
            else str += left;
            str += " " + operator + " ";
            if (operator == Op.TIMES && right instanceof Plus)
                str += "(" + right + ")";
            else str += right;
            return str;
        }
        return left + " " + right + " " + operator;
    }
}

class Plus extends BinaryOperation {
    Plus(Expression e1, Expression e2) {
        super(e1,Op.PLUS,e2);
    }
}

class Times extends BinaryOperation {
    Times(Expression e1, Expression e2) {
        super(e1,Op.TIMES,e2);
    }
}