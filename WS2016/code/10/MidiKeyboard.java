import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiUnavailableException;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;

class MidiKeyboard implements AutoCloseable {
  private static final Map<Character,Integer> notes;
  static {                                                    // <1>
    notes = new HashMap<Character, Integer>();
    notes.put('c', 60);
    notes.put('d', 62);
    notes.put('e', 64);
    notes.put('f', 65);
    notes.put('g', 67);
    notes.put('a', 69);
    notes.put('b', 71);
  }
  private Synthesizer s;
  public MidiKeyboard() throws MidiUnavailableException {
    s = MidiSystem.getSynthesizer();
    s.open();
  }
  public void close() {
    s.close();
  }
  public void playNote(int note, int vel, int dur) {
    MidiChannel[] channels = s.getChannels();
    channels[0].noteOn(note, vel);
    try {
      Thread.sleep(dur);
    } catch (InterruptedException ie) { /* ignore */ }
    channels[0].noteOff(note);
  }
  public static void main(String[] args) {
    try (MidiKeyboard kb = new MidiKeyboard()) {
      Scanner sc = new Scanner(System.in);
      String line;
      while((line = sc.nextLine()) != null) {
        if ("exit".equals(line)) {
          System.exit(0);
        }
        for(char c : line.toCharArray()) {
          if (notes.containsKey(c)) {
            kb.playNote(notes.get(c), 60, 300);
          } else if (c == ' ') {
            Thread.sleep(300);
          }
        }
      } // end while
    } catch (MidiUnavailableException me) {
      System.out.println("Midi playback is unavailable, sorry.");
    } catch (InterruptedException ie) {
      // ignore
    }
  } // end main
}
