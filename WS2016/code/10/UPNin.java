import java.util.*;
import java.io.*;

class UPNin {

    Stack<Integer> stack = new Stack<Integer>();

    public static void main(String[] args) {
        UPN upn = new UPN();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        try {
            while((line = in.readLine()) != null) {
                line = line.trim();
                upn.run(line.split("\\s"));
            }
        } catch (IOException e) {
            System.out.println("No input available!");
            System.exit(1);
        }
        for(Integer i : upn.stack) System.out.print(i + " ");
        System.out.println();
        System.exit(0);

    }

    void run(String[] words) {
        for(String word : words)
            try {
                eval(stack,word);
            } catch (EmptyStackException e) {
                System.out.println("Stack underflow!");
                System.exit(1);
            }
    }

    void eval(Stack<Integer> s, String word) {
        switch(word) {
            case "swap":
                Integer y = s.pop();
                Integer x = s.pop();
                s.push(y);
                s.push(x);
                break;
            case "dup":
                s.push(s.peek());
                break;
            case "drop":
                s.pop();
                break;
            case "neg":
                s.push(-s.pop());
                break;
            case "+":
                s.push(s.pop() + s.pop());
                break;
            case "-":
                eval(s,"neg");
                eval(s, "+");
                break;
            case "*":
                s.push(s.pop() * s.pop());
                break;
            case "/":
                eval(s,"swap");
                s.push(s.pop() / s.pop());
                break;
            case "%":
                eval(s,"swap");
                s.push(s.pop() % s.pop());
                break;
            default:
                try {
                    s.push(Integer.parseInt(word));
                } catch (NumberFormatException e) {
                    s.push(0);
                }
        }
    }
}