import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

class HoloceneCalendar extends GregorianCalendar {
    public HoloceneCalendar() {
      super();
      updateToHolo();
    }
    private void updateToHolo() {
      set(Calendar.YEAR, get(Calendar.YEAR) + 10000);
    }
    public HoloceneCalendar(int year, int month, int dayOfMonth) {
      super(year, month, dayOfMonth);
    }
    public HoloceneCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute) {
      super(year, month, dayOfMonth, hourOfDay, minute);
    }
    public HoloceneCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute, int second) {
      super(year, month, dayOfMonth, hourOfDay, minute, second);
    }
    public HoloceneCalendar(Locale aLocale) {
      super(aLocale);
      updateToHolo();
    }
    public HoloceneCalendar(TimeZone zone) {
      super(zone);
      updateToHolo();
    }
    public HoloceneCalendar(TimeZone zone, Locale aLocale) {
      super(zone, aLocale);
      updateToHolo();
    }
    public static void main(String[] args) throws InterruptedException {
      while(true) {
        String timeString = new HoloceneCalendar().getTime().toString();
        System.out.print(timeString);
        Thread.sleep(100);
        for(int i = 0; i < timeString.length(); i++) {
          System.out.print('\b'); // ASCII code for backspace
        }
      }
    }
}
