= 5. Woche: Fragen und Aufgaben zu OOP
Dominikus Herzberg, Nadja Krümmel, Christopher Schölzel
V0.91, 2017-03-16
:toc:
:icons: font
:solution:
:source-highlighter: coderay
:sourcedir: ./code/5

////
In Woche 4 habe ich eingeführt:

Klasse (ohne Vererbung)
* mit statischen Members (Feldern und Methoden)
* mit nicht-statischen Members (Feldern und Methoden)
* Konstruktoren

Siehe dazu auch die beispielhafte Haus-Klasse in Moodle bzw. in git 4.Woche.Plan.adoc.

Die Herausforderung ist, Aufgaben zu finden, wo statische bzw. nicht-statische Methoden Sinn machen, d.h. gut begründet sind.

Interessant sind alle Aufgaben, in denen man dieses Setting aus Feldern und Methoden aufbaut und über die JShell exploriert bzw. testet. Ein Beispiel möge die Auto-Aufgabe bieten.

////

== Naive gcd-Implementierung (V)

Erstellen Sie eine Methode namens `gcd`, die den größten gemeinsamen Teiler (_greatest common divisor_) zweier Ganzzahlen bestimmt. Die Implementierung soll in einer Schleife alle Zahlen durchprobieren (begrenzen Sie den Suchraum sinnvoll), bis ein Teiler gefunden ist. Sie dürfen, falls hilfreich, Funktionen der Klasse `Math` verwenden.

.Beispiel einer Interaktion
----
jshell> gcd(5,10)
$36 ==> 5

jshell> gcd(-6,-15)
$37 ==> 3

jshell> gcd(16,56)
$38 ==> 8
----

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/reduce.java[]
----
endif::solution[]



== Syntax-Definitionen

=== For-Schleife

Benennen Sie die drei Teile des Kopfs einer For-Schleife. Welche dieser Teile dürfen leer bleiben?

----
//     [1]       [2]    [3]
for(int i = 0; i < 100; i++) {
  // Körper
}
----

ifdef::solution[]
.Lösung
. Initialisierung
. Fortsetzungsbedingung (Achtung: Nicht _Abbruchbedingung_!)
. Schrittanweisung

Initialisierung und Schrittanweisung dürfen leer bleiben. Die Semikola müssen aber trotzdem stehen bleiben.

Beispiel:
----
for(;i < 100;) {
  // Körper
}
----
endif::solution[]

=== Methodendefinition

Welche der folgenden Begriffe beschreiben Teile einer Methodendefinition? Sortieren Sie die unpassenden Begriffe aus und ordnen Sie die übrig bleibenden Begriffe in ihrer Reihenfolge. Gibt es in der Liste Überbegriffe, Synonyme und Unterbegriffe? Stellen Sie die Beziehungen her!

* Datentyp
* Hand
* Parameter
* Name
* Konstruktor
* Return-Statement
* Variable
* Kopf
* Rückgabetyp
* Argument
* Körper
* Fuß
* Bezeichner
* Rumpf
* Signatur
* Herz
* Rückgabeanweisung
* Initialisierung

ifdef::solution[]
.Lösung
* Kopf
  ** Rückgabetyp
  ** Signatur
    *** Name oder Bezeichner
    *** Parameter
* Körper oder Rumpf
  ** Rückgabeanweisung oder Return-Statement

Erklärungen zu falschen Begriffen:

* Der Begriff Datentyp ist zu allgemein. Es gibt in einer Methodendefinition sowohl den Rückgabetyp als auch die Parametertypen.
* Die Begriffe Hand, Fuß und Herz werden nicht im Kontext von Methoden verwendet.
* Konstruktoren sind besondere Formen von Objektmethoden, aber ein Konstruktor ist kein Bestandteil einer Methode.
* Parameter sind zwar so etwas ähnliches wie Variablen, aber mit dem Begriff "Variable" bezeichnet man eine lokale Variable oder das Feld eines Objektes oder einer Klasse.
* Der Begriff Argument wird oft synonym mit dem Begriff Parameter verwendet. Wenn man es ganz genau nimmt ist ein Argument aber ein konkreter Wert, den ein Parameter beim Aufruf der Methode annimmt.
* Eine Methode wird nicht initialisiert. Der Begriff Initialisierung kommt nur im Zusammenhang mit Variablen und Feldern vor.
endif::solution[]






////
OOP-Aufgaben
- Brücke macht Dominikus
- Vektorrechnung
- Einfache Aufgaben mit Klassen
- Objekt-Arrays
- Stack (bzw. einfach verkettete Liste)
- static vs nicht-static, wann kann ich was aufrufen
- noch keine visibility (aber private kann schon auftauchen)
- struktur von Methode/Klasse/Schleife abfragen

- 2D-/3D-Punkte
- Telefonbuch?
- Studentenverwaltung

////


////
* Fraction
* Stapel
*

////
